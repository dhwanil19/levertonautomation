####################################################################################################################
#                      Leverton Automation Task 2 - API Automation								   				   #		
#Description - For the given URI, use GET method and search a user given country in the response obtained.		   #
#			   Details such as Capital, Region, Numeric code and Currency is displayed for a valid country         #	
#Scenarios covered - 1) 3 Valid countries											                               #	
#					 2) One invalid country							   											   #	
#Comments - Scenario with invalid country fails. Check extent report for more details.							   #									   			
#Created By - Dhwanil Shah																						   #			
#Email - dhwanilshah1992@gmail.com																				   #	
####################################################################################################################
Feature: Check and Fetch country details using API 

@Part2
Scenario Outline: To check whether a country is present in the REST response 

	When The user hits URI for getting countries data 
	Then The requested data is returned 
	When Country "<Country Name>" is searched in response and the country is present 
	Then Country name and details are displayed 
	
	Examples: 
		|Country Name|
		|India	     |
		|Germany     |
		|Turkey      |
		|InvalidCountry| 

