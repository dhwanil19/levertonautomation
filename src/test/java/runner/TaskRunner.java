package runner;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.cucumber.listener.ExtentProperties;
import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features", dryRun = false, glue = "stepDefinitions", monochrome = true, tags = {
"@Part1,@Part2" }, plugin = { "com.cucumber.listener.ExtentCucumberFormatter:" })
public class TaskRunner {

	@BeforeClass
	public static void setup() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("ddMMMyy_hh-mm-ss");
		String timestamp = format.format(date);

		String resultPath = "./Results/Run_" + timestamp;
		File file = new File(resultPath);
		file.mkdirs();

		String screenShotFolder = resultPath + "//Screenshots";
		File sc = new File(screenShotFolder);
		sc.mkdir();
		System.setProperty("ScreenshotFolder", screenShotFolder + "/");

		ExtentProperties extentProperties = ExtentProperties.INSTANCE;
		extentProperties.setReportPath(resultPath + "/CucumberReport.html");
	}

	@AfterClass
	public static void tearDown() {
		Reporter.loadXMLConfig(new File("src/test/resources/extent-config.xml"));
	}

}