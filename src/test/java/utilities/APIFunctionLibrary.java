package utilities;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class APIFunctionLibrary {

	int statusCode = 0;
	private String capital;
	private int numericCode;
	private String region;
	private String currencies;

	public boolean findCountryInResponse(String response, String countryName) throws Exception {
		JSONObject jsonObj = null;
		JSONParser parse = new JSONParser();
		JSONArray json = (JSONArray) parse.parse(response);

		boolean isCountryPresent = false;

		for (int i = 0; i < json.size(); i++) {
			jsonObj = (JSONObject) json.get(i);
			String country = (String) jsonObj.get("name");
			if (country.equals(countryName)) {
				isCountryPresent = true;
				setCapital(jsonObj.get("capital").toString());
				setRegion(jsonObj.get("region").toString());
				setNumericCode(Integer.parseInt(jsonObj.get("numericCode").toString()));

				//Remove square braces from currencies
				String currency = jsonObj.get("currencies").toString();
				currency = currency.replace("[", "");
				currency = currency.replace("]", "");
				setCurrencies(currency);
			}
		}

		return isCountryPresent;
	}

	public String getRESTResponse(String uri) throws Exception {
		String responseBody = "";
		URL url = new URL("http://restcountries.eu/rest/v1");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.connect();
		statusCode = connection.getResponseCode();
		Scanner sc = new Scanner(url.openStream());
		while (sc.hasNext()) {
			responseBody += sc.nextLine();
		}
		sc.close();

		return responseBody;
	}

	public int getRESTResponseCode() {
		// TODO Auto-generated method stub
		return statusCode;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public int getNumericCode() {
		return numericCode;
	}

	public void setNumericCode(int numericCode) {
		this.numericCode = numericCode;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCurrencies() {
		return currencies;
	}

	public void setCurrencies(String currencies) {
		this.currencies = currencies;
	}

}