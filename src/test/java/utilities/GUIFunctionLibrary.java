package utilities;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GUIFunctionLibrary {


	WebDriver driver;
	WebDriverWait wait;
	public String captureScreenshot(WebDriver driver,String folderPath, String screenShotName) throws Exception
	{

		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String dest = folderPath+"\\"+screenShotName+".png";
		File destination = new File(dest);
		FileUtils.copyFile(source, destination);

		return dest;
	}

	public boolean initializeTestBrowser() throws Exception {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 40);
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		return true;
	}

	public String navigateToUrl(String string) throws Exception {
		driver.get("https://platform-dev.leverton.de/login/auth");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
		captureScreenshot(driver, System.getProperty("ScreenshotFolder"), "HomePage");
		return driver.getCurrentUrl();
	}

	public boolean enterCredentials(String uname, String pword) {
		WebElement txtBox_username = driver.findElement(By.id(ObjectRepository.txtBox_username));
		txtBox_username.sendKeys(uname);

		WebElement txtBox_password = driver.findElement(By.id(ObjectRepository.txtBox_password));
		txtBox_password.sendKeys(pword);

		WebElement btn_Login = driver.findElement(By.xpath(ObjectRepository.btn_login));
		btn_Login.click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className(ObjectRepository.errorMsg)));
		return true;
	}

	public String getErrorMessage() throws Exception {
		WebElement error_msg = driver.findElement(By.className(ObjectRepository.errorMsg));
		captureScreenshot(driver, System.getProperty("ScreenshotFolder"), "ErrorMessage");
		return error_msg.getText();
	}

	public String getCurrentUrl() {
		// TODO Auto-generated method stub
		return driver.getCurrentUrl();

	}

	public void closeBrowser() {
		// TODO Auto-generated method stub
		driver.quit();
	}

}
