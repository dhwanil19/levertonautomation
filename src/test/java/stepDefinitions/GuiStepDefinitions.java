package stepDefinitions;

import com.cucumber.listener.Reporter;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import utilities.GUIFunctionLibrary;

public class GuiStepDefinitions {

	GUIFunctionLibrary lib = new GUIFunctionLibrary();

	@Given("^Initial browser setup is completed$")
	public void initial_browser_setup_is_completed() throws Throwable {
		boolean result = lib.initializeTestBrowser();
		if (result == true) {
			System.out.println("Chrome Browser Initialized Successfully");
			Reporter.addStepLog("Chrome Browser Initialized Successfully");
		} else {
			System.out.println("Unable to initialize Chrome Browser");
			Reporter.addStepLog("Unable to initialize Chrome Browser");
			Assert.assertEquals(true, false);
		}

	}

	@Given("^User navigates to Leverton url$")
	public void user_navigates_to_Leverton_url() throws Throwable {

		String url = lib.navigateToUrl("https://platform-dev.leverton.de/login/auth");
		if (url.equals("https://platform-dev.leverton.de/login/auth")) {
			System.out.println("Navigated to homepage - https://platform-dev.leverton.de/login/auth");
			Reporter.addStepLog("Navigated to homepage - https://platform-dev.leverton.de/login/auth");

		} else {
			System.out.println("Unable to navigate to homepage. Current url - " + url);
			Reporter.addStepLog("Unable to navigate to homepage. Current url - " + url);
			Assert.assertEquals(true, false);
		}
	}

	@When("^The user enters \"([^\"]*)\" and \"([^\"]*)\"$")
	public void the_user_enters_and(String username, String password) throws Throwable {
		boolean result = lib.enterCredentials(username, password);
		if (result == true) {
			System.out.println("Credentials entered successfully. Username - " + username + " Password " + password);
			Reporter.addStepLog("Credentials entered successfully. Username - " + username + " Password " + password);
		} else {
			System.out.println("Unable to enter credentials");
			Reporter.addStepLog("Unable to enter credentials");
			Assert.assertEquals(true, false);
		}

	}

	@Then("^Error text is displayed on the page$")
	public void error_text_is_displayed_on_the_page() throws Throwable {
		String errorMsg = lib.getErrorMessage();
		if (errorMsg.contains("Sorry")) {
			System.out.println("Error message displayed - " + errorMsg);
			Reporter.addStepLog("Error message displayed - " + errorMsg);
		} else {
			System.out.println("Error message as expected. Message text - " + errorMsg);
			Reporter.addStepLog("Error message as expected. Message text - " + errorMsg);
			Assert.assertEquals(true, false);
		}
	}

	@Then("^User is unable to navigate to another page$")
	public void user_is_unable_to_navigate_to_another_page() throws Throwable {
		String errorUrl = lib.getCurrentUrl();
		if (errorUrl.contains("error")) {
			System.out.println("User is on error page with URL- " + errorUrl);
			Reporter.addStepLog("User is on error page with URL- " + errorUrl);
		} else {
			System.out.println("User not on error page. Current URL- " + errorUrl);
			Reporter.addStepLog("User not on error page. Current URL- " + errorUrl);
			Assert.assertEquals(true, false);
		}

		lib.closeBrowser();
	}
}
