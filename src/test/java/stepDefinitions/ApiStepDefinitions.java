package stepDefinitions;

import com.cucumber.listener.Reporter;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import utilities.APIFunctionLibrary;

public class ApiStepDefinitions {

	boolean isCountryPresent;
	String responseBody = "";
	APIFunctionLibrary lib = new APIFunctionLibrary();
	private String countryName;

	@When("^The user hits URI for getting countries data$")
	public void the_user_hits_URI_for_getting_countries_data() throws Throwable {

		responseBody = lib.getRESTResponse("http://restcountries.eu/rest/v1");
		Reporter.addStepLog("URI hit successfully");
	}

	@Then("^The requested data is returned$")
	public void the_requested_data_is_returned() throws Throwable {

		int respCode = lib.getRESTResponseCode();
		if (respCode == 200) {
			System.out.println("Successfully hit URI. Status code is - " + respCode);
			Reporter.addStepLog("Successfully hit URI. Status code is - " + respCode);
		} else {
			System.out.println("Unable to hit URI. Please check URI. Status code - " + respCode);
			Reporter.addStepLog("Unable to hit URI. Please check URI. Status code - " + respCode);
			Assert.assertEquals(true, false);
		}

	}

	@When("^Country \"([^\"]*)\" is searched in response and the country is present$")
	public void country_is_searched_in_response_and_the_country_is_present(String countryName) throws Throwable {

		this.countryName = countryName;
		isCountryPresent = lib.findCountryInResponse(responseBody, countryName);
		if (isCountryPresent == true) {
			System.out.println("Search successful for country - " + countryName);
			Reporter.addStepLog("Search successful for country - " + countryName);
		} else {
			System.out.println("Country " + "\'"+ countryName+"\'" + " not found in response");
			Reporter.addStepLog("Country " + "\'"+ countryName+"\'" + " not found in response");
			Assert.assertEquals(true, false);
		}
	}

	@Then("^Country name and details are displayed$")
	public void country_name_and_details_are_displayed() throws Throwable {

		if (isCountryPresent == true) {
			System.out.println("Country " + "\'" + countryName + "\'" + " is present in the response");
			System.out.println("Capital of the country -" + lib.getCapital());
			System.out.println("Region of the country - " + lib.getRegion());
			System.out.println("Numeric code of the country - " + lib.getNumericCode());
			System.out.println("Currency of the country - " + lib.getCurrencies());

			Reporter.addStepLog("Country " + "\'" + countryName + "\'" + " is present in the response");
			Reporter.addStepLog("Capital of the country -" + lib.getCapital());
			Reporter.addStepLog("Region of the country - " + lib.getRegion());
			Reporter.addStepLog("Numeric code of the country - " + lib.getNumericCode());
			Reporter.addStepLog("Currency of the country - " + lib.getCurrencies());

		} else {
			Reporter.addStepLog("Country " + "\'" + countryName + "\'" + " is not present in the response");
			System.out.println("Country " + "\'" + countryName + "\'" + " is not present in the response");
			Assert.assertEquals(true, false);
		}
	}

}
