####################################################################################################################
#                      Leverton Automation Task 1 - GUI Automation								   				   #		
#Description - User logs in to the given url and enters invalid credentials. Validate the error for the same	   #	
#Scenarios covered - 1) Random Alphanumeric username and password												   #	
#					 2) Special characters username and Random Alphanumeric password 							   #	
#					 3) Random Alphanumeric username and Special characters password 							   #	
#					 4) Special characters username and password                                             	   #																   			
#Created By - Dhwanil Shah																						   #			
#Email - dhwanilshah1992@gmail.com																				   #	
####################################################################################################################
Feature: Check Login functionality of Leverton Platform 

@Part1 
Scenario Outline: To validate login failure with random credentials 

	Given Initial browser setup is completed 
	And   User navigates to Leverton url 
	When  The user enters "<InvalidUsername>" and "<InvalidPassword>" 
	Then  Error text is displayed on the page 
	And   User is unable to navigate to another page 
	
	Examples: 
		|InvalidUsername|InvalidPassword|
		|inval_user     |inval_pwd      |
		|#_)(           |inval_pwd      |
		|inval_user     |#####%$        |
		|^&*()          | !@#$%         | 
		